--[[
Allows automatically translating sequences of characters to other sequences
within special inline blocks of the form

    @<context>"<text>"

To define the translation contexts, a block of the form

    <!-- TR <context>
    "<pattern>" "<replacement>"
    ...
    -->

is used.

A context is any string of alphanumeric characters.
]]

local util = require('util')

local start_pat = '^%s*<!%-%-%s*TR%s+(%w+)%s*$'
local repl_pat = '^%s*"([^"]+)"%s+"([^"]*)"%s*$'
local end_pat = '^%s*%-%->%s*$'
local tr_pat = '@(%w+)"([^"]*)"'

local mode = 'normal'
local contexts = {}
local context

local function do_tr(name, text)
  text = util.unescape(text)
  local ctx = contexts[name]
  if not ctx then error('no context ' .. name) end
  local r = ''
  local i = 1
  while i <= #text do
    for j = #ctx, 1, -1 do
      local t = ctx[j]
      if t[1] then
        local a = text:sub(i, i + j - 1)
        local b = t[a]
        if b then
          r = r .. b
          i = i + #a
          goto fin
        end
      end
    end

    r = r .. text:sub(i, i)
    i = i + 1
    ::fin::
  end
  return r
end

for line in io.lines() do
  local r = {}
  if mode == 'normal' then
    if util.find(line, start_pat, r) then
      mode = 'tr'
      if not contexts[r[1]] then contexts[r[1]] = {} end
      context = contexts[r[1]]
    else
      print((line:gsub(tr_pat, do_tr)))
    end
  elseif util.find(line, repl_pat, r) then
    local a, b = util.unescape(r[1]), util.unescape(r[2])
    while #context < #a do table.insert(context, {}) end
    context[#a][1] = true
    context[#a][a] = b
  elseif util.find(line, end_pat, r) then
    mode = 'normal'
  end
end
