local util = {}

local escape_tr = {}
escape_tr['\\'] = '\\'
escape_tr['\a'] = '\a'
escape_tr['\f'] = '\f'
escape_tr['\n'] = '\n'
escape_tr['\r'] = '\r'
escape_tr['\t'] = '\t'
escape_tr['\v'] = '\v'

function util.unescape(s, quote)
  quote = quote or '"'
  s = s:gsub('\\([XxUu]){([^}]+)}', function(c, x)
      x = tonumber(x, 16)
      if c == 'X' or c == 'x' then return string.char(x)
      else return utf8.char(x) end
  end)
  s = s:gsub('\\(.)', function(c)
      if c == "'" then return quote
      else return escape_tr[c] or c end
  end)
  return s
end

function util.find(s, pat, r, init, plain)
  local t = {string.find(s, pat, init, plain)}
  if not t[1] then return false end
  r.i = t[1]
  r.j = t[2]
  for i = 3, #t do r[i - 2] = t[i] end
  return true
end

return util
