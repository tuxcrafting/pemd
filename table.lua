--[[
This adds tables of the form:

    <!-- T-BEGIN -->
    ...
    <!-- T-END -->

Table header and footer are optionally delimited by more directives,
as well as captions:

    <!-- T-BEGIN -->
    caption...
    <!-- T-CAPTION -->
    header...
    <!-- T-HEAD -->
    body...
    <!-- T-FOOT -->
    footer...
    <!-- T-END -->

Cells are started with <td> or <th>, and rows are started with <tr>. They do
not have to be terminated with an end tag.

Additionally, rowspan and colspan can be specified by adding "rN"
and/or "cN" in the td and th tags, like <td r3 c2>.
]]

local util = require('util')

local directive_pat = '^%s*<!%-%-%s*T%-(%w+)%s*%-%->%s*$'

local mode = 0
local current
local buffers

local function process_section(text)
  local rows = {}
  local row
  local cell
  local i = 1

  local function do_cell(tag, rowspan, colspan)
    if not row then error('no rows') end
    cell = {
      tag = tag,
      text = '',
      rowspan = rowspan,
      colspan = colspan
    }
    table.insert(row, cell)
  end

  local function do_text(s)
    if not cell then error('no cells') end
    cell.text = cell.text .. s
  end

  while i <= #text do
    local r = {}
    if util.find(text, '%s+$', r, i) then
      i = r.j + 1
    elseif util.find(text, '^%s*<tr>', r, i) then
      row = {}
      table.insert(rows, row)
      cell = nil
      i = r.j + 1
    elseif util.find(text, '^%s*<(t[hd])>%s*', r, i) then
      do_cell(r[1], 1, 1)
      i = r.j + 1
    elseif util.find(text, '^%s*<(t[hd]) r([0-9]+)>%s*', r, i) then
      do_cell(r[1], tonumber(r[2]), 1)
      i = r.j + 1
    elseif util.find(text, '^%s*<(t[hd]) c([0-9]+)>%s*', r, i) then
      do_cell(r[1], 1, tonumber(r[2]))
      i = r.j + 1
    elseif util.find(text, '^%s*<(t[hd]) r([0-9]+) c([0-9]+)>%s*', r, i) then
      do_cell(r[1], tonumber(r[2]), tonumber(r[3]))
      i = r.j + 1
    elseif util.find(text, '^%s*<(t[hd]) c([0-9]+) r([0-9]+)>%s*', r, i) then
      do_cell(r[1], tonumber(r[3]), tonumber(r[2]))
      i = r.j + 1
    elseif util.find(text, '^([^<>%s]+)', r, i) then
      do_text(r[1])
      i = r.j + 1
    else
      do_text(text:sub(i, i))
      i = i + 1
    end
  end

  for _, row in ipairs(rows) do
    print('<tr>')
    for _, cell in ipairs(row) do
      local t = cell.tag
      if cell.rowspan ~= 1 then t = t .. ' rowspan="' .. cell.rowspan .. '"' end
      if cell.colspan ~= 1 then t = t .. ' colspan="' .. cell.colspan .. '"' end
      print('<' .. t .. '>')
      print(cell.text)
      print('</' .. cell.tag .. '>')
    end
    print('</tr>')
  end
end

for line in io.lines() do
  local r = {}
  if util.find(line, directive_pat, r) then
    local dir = r[1]
    if dir == 'BEGIN' then
      if mode ~= 0 then error('nested table') end
      mode = 1
      buffers = {
        caption = {},
        head = {},
        body = {},
        foot = {}
      }
      current = {}
    elseif dir == 'CAPTION' then
      if mode ~= 1 then error('invalid mode') end
      mode = 2
      buffers.caption = current
      current = {}
    elseif dir == 'HEAD' then
      if mode == 0 or mode > 2 then error('invalid mode') end
      mode = 3
      buffers.head = current
      current = {}
    elseif dir == 'FOOT' then
      if mode == 0 or mode > 3 then error('invalid mode') end
      mode = 4
      buffers.body = current
      current = {}
    elseif dir == 'END' then
      if mode == 0 then error('no matching begin')
      elseif mode == 4 then buffers.foot = current
      else buffers.body = current end
      mode = 0

      print('<table>')
      if #buffers.caption ~= 0 then
        print('<caption>')
        for _, c_line in ipairs(buffers.caption) do print(c_line) end
        print('</caption>')
      end
      if #buffers.head ~= 0 then
        print('<thead>')
        process_section(table.concat(buffers.head, '\n'))
        print('</thead>')
      end
      if #buffers.body ~= 0 then
        print('<tbody>')
        process_section(table.concat(buffers.body, '\n'))
        print('</tbody>')
      end
      if #buffers.foot ~= 0 then
        print('<tfoot>')
        process_section(table.concat(buffers.foot, '\n'))
        print('</tfoot>')
      end
      print('</table>')
    else
      print(line)
    end
  elseif mode == 0 then
    print(line)
  else
    table.insert(current, line)
  end
end

if mode ~= 0 then error('unterminated table') end
