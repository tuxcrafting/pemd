<!-- INCLUDE "xsampa.md" -->

American English "pronunciation" [@xs"pr\\@%nVn.si.\'eI.S@n"].

<style>table, th, td {border: 1px solid black;}</style>

<!-- T-BEGIN -->
Hello **World**
<!-- T-CAPTION -->
<tr> <th> Foo <th> Bar <th> Baz
<!-- T-HEAD -->
<tr> <td> 1 <td r2 c2> 2
<tr> <td> 3
<tr> <td r2> 4 <td c2> 5
<tr> <td> 6 <td> 7
<!-- T-FOOT -->
<tr> <th c3> *Formatting*
<!-- T-END -->
