--[[
Adds a directive of the form

    <!-- INCLUDE "<path>" -->

that includes the specified file literally in the output.
]]

local util = require('util')

local pat = '^%s*<!%-%-%s*INCLUDE%s+"([^"]+)"%s*%-%->%s*$'

for line in io.lines() do
  local r = {}
  if util.find(line, pat, r) then
    for inc_line in io.lines(util.unescape(r[1])) do
      print(inc_line)
    end
  else
    print(line)
  end
end
